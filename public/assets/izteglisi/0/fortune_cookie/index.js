function template(locals) {
    var buf = [];
    var jade_mixins = {};
    var jade_interp;;
    var locals_for_with = (locals || {});
    (function (Math, first_name, image, quotes) {
        var item = quotes[Math.floor((Math.random() * quotes.length) + 0)].value;
        buf.push("<!DOCTYPE html><html lang=\"en\"><head><link rel=\"stylesheet\" href=\"//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=cyrillic\" attr><meta charset=\"utf-8\"></head><body style=\"margin:0;padding:0;\"> <div" + (jade.attr("style", "text-align:center; height:315px; width:620px; font-family:'Open Sans'; background: transparent url('//s3-eu-west-1.amazonaws.com/arpecop.com/cdn/" + image + ".jpg')  no-repeat center center;      -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: table;", true, true)) + " class=\"coverimage\"><div style=\"-webkit-font-smoothing: antialiased;font-size:25px;  padding:5px;   width:70%; -webkit-transform: rotate(-2deg);    display: table-cell; vertical-align: middle;\" class=\"text\"><div style=\"background-color:#FFF;  width:70%; margin:auto; line-height:20px; padding:20px;-webkit-box-shadow: 0 8px 6px -6px black;\"> " + (jade.escape((jade_interp = first_name) == null ? '' : jade_interp)) + " , " + (jade.escape((jade_interp = item) == null ? '' : jade_interp)) + " </div></div></div></body></html>");
    }.call(this, "Math" in locals_for_with ? locals_for_with.Math : typeof Math !== "undefined" ? Math : undefined, "first_name" in locals_for_with ? locals_for_with.first_name : typeof first_name !== "undefined" ? first_name : undefined, "image" in locals_for_with ? locals_for_with.image : typeof image !== "undefined" ? image : undefined, "quotes" in locals_for_with ? locals_for_with.quotes : typeof quotes !== "undefined" ? quotes : undefined));;
    return buf.join("");
}