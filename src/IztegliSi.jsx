import React, { Component } from "react";

import axios from "axios";

import Quiz from "./izteglisi/Quiz.jsx";
import Random from "./izteglisi/Random.jsx";
import Script from "react-load-script";
import shuffle from "lodash/shuffle";

import Responsive from "react-responsive";

const Desktop = props => <Responsive {...props} minWidth={992} />;
const Tablet = props => <Responsive {...props} minWidth={768} maxWidth={991} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

const colors = [
  "#db2828",
  "#f2711c",
  "#fbbd08",
  "#b5cc18",
  "#21ba45",
  "#00b5ad",
  "#2185d0",
  "#6435c9",
  "#e03997",
  "#1b1c1d"
];
//  <img alt="" src={`/images/izteglisi/${props.value.slug}_sm.jpg`} />
const Iztegli = props => (
  <React.Fragment>
    <Desktop>
      <a
        href={`https://arpecop.gitlab.io/iztegli/${props.id}`}
        style={{ backgroundColor: shuffle(colors)[0], width: "19%" }}
      >
        <div className="ui fluid rounded image">
          <div className="ui label">{props.value.title}</div>
          <div className="ui description">{props.value.desc}</div>
        </div>
      </a>
    </Desktop>
    <Tablet>
      <a
        href={`https://arpecop.gitlab.io/iztegli/${props.id}`}
        style={{ width: "32.333333%", backgroundColor: shuffle(colors)[0] }}
      >
        <div className="ui fluid rounded image">
          <div className="ui label">{props.value.title} </div>
          <div className="ui description">{props.value.desc}</div>
        </div>
      </a>
    </Tablet>
    <Mobile>
      <a
        href={`https://arpecop.gitlab.io/iztegli/${props.id}`}
        style={{ width: "49%", backgroundColor: shuffle(colors)[0] }}
      >
        <div className="ui fluid rounded image">
          <div className="ui label">{props.value.title}</div>
          <div className="ui description">{props.value.desc}</div>
        </div>
      </a>
    </Mobile>
  </React.Fragment>
);

 
export default class IztegliSi extends Component {
  constructor(props) {
    super(props);
 
    var urlid = window.location.href
      .split("/")
      .reverse()[0]
      .split("?")[0];
    this.state = {
      api:
        process.env.NODE_ENV === "production"
          ? "//pouchdb.herokuapp.com/db/"
          : "//pouchdb.herokuapp.com/db/",
      title: "",
      _id: urlid,
      desc: "",
      back: "",

      fb: {
        name: "izvestie",
        appid: "181361935494",
        share: "Сподели във Facebook",
        login: "Продължи към резултата",
        db: "bg"
      },
      quizes: [],
      data: {}
    };
  }

  componentWillMount() {
    axios
      .all([
        axios.get(
          `${this.state.api}_design/i/_view/iztegli${
            this.state.fb.db
          }?limit=10&descending=true&include_docs=true${
            this.state._id ? `&start_key="${this.state._id}"` : ""
          }`
        )
      ])
      .then(
        axios.spread(quizesList => {
          let type;
          if (quizesList.data.rows[0].doc.quotes) {
            type = "random";
          } else if (quizesList.data.rows[0].doc.questions) {
            type = "quiz";
          } else {
            type = "unknown";
          }
          if (quizesList.data.rows[0].doc.quiz) {
            this.setState({
              quizes: shuffle(quizesList.data.rows),
              title: quizesList.data.rows[0].doc.quiz.title,
              desc: quizesList.data.rows[0].doc.quiz.title,
              isLoading: true,
              data: quizesList.data.rows[0].doc,
              type,
              quotes: quizesList.data.rows[0].doc.quotes
            });
          } else {
            this.setState({
              back: "/images/izteglisi.jpg",
              title: "Фабрика за позитивно мислене",
              desc: "...Защото само и единствено то може да промени съдбата  ",
              quizes: quizesList.data.rows
            });
          }
        })
      )
      .catch(error => {
        this.setState({
          back: "/images/izteglisi.jpg",
          title: "Фабрика за позитивно мислене",
          desc: "...Защото само и единствено то може да промени съдбата  "
        });
      });
  }
  render() {
    const QuizHtml =
      this.state.type === "quiz" ? (
        <Quiz
          type={this.state.type}
          fb={this.state.fb}
          data={this.state.data}
        />
      ) : (
        ""
      );
    const RandomHtml =
      this.state.type === "random" || this.state.type === "entername" ? (
        <Random
          type={this.state.type}
          fb={this.state.fb}
          data={this.state.data}
        />
      ) : (
        ""
      );
    const childElements = this.state.quizes.map(el => (
      <Iztegli value={el.value} key={el.id} id={el.id} />
    ));

    return (
      <div>
        <Script url="//st-n.ads3-adnow.com/js/adv_out.js" />
        <Script url="/fb/izteglisicom.js" />
        {this.state.id}
        {RandomHtml}

        {QuizHtml}
        <div className="separator">Други приложения и тестове:</div>
        <div id="applist">{childElements}</div>
      </div>
    );
  }
}
